'use strict';

describe('copyFile', () => {
    const {resolve} = require('path');
    const {unlinkSync, existsSync} = require('fs');
    const copyFile = require('../lib/utils/copyFile');

    const SOURCE_FILE_LG = resolve('./spec/fixtures/source_lg.txt');
    const SOURCE_FILE_SM = resolve('./spec/fixtures/source_sm.txt');
    const DEST_FILE = resolve('./tmp/dest.tmp');

    beforeEach(() => {
        if (existsSync(DEST_FILE)) {
            unlinkSync(DEST_FILE);
        }
    });

    it('Must copy file', () => {
        copyFile(SOURCE_FILE_LG, DEST_FILE);
        expect(existsSync(DEST_FILE)).toBeTruthy();

        copyFile(SOURCE_FILE_SM, DEST_FILE);
        expect(existsSync(DEST_FILE)).toBeTruthy();
    });
});
