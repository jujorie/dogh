'use strict';

describe('fileUtils', () => {
    const {resolve} = require('path');
    const {processFile} = require('../lib/utils/fileUtils');
    const copyFile = require('../lib/utils/copyFile');
    const SOURCE_PATH = resolve('spec/fixtures');
    const DEST_PATH = resolve('tmp');
    let callback;

    beforeAll(() => {
        callback = jasmine.createSpy('callback', (src, dst) => {
            copyFile(src, dst);
        });
    });

    it('Must to process file', () => {
        processFile(SOURCE_PATH, DEST_PATH, callback);
        expect(callback).toHaveBeenCalledTimes(2);
    });
});
