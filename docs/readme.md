# Commands

Available commands

- [install](#install)
- [uninstall](#uninstall)

## Install

To install the git hooks 

```shell
$ dogh install
```

this command creates an _scripts/githooks_ folder with pre setted git hooks

### pre-commit Hook

This runs a _lint_ command using a 

```shell
$ npm run lint
```

if the linter fails then the commit process is aborted

### pre-push Hook

This runs a _test_ command using 

```shell
$ npm test
```

if the test fails then the push command is aborted

### Custom commands

any file  dropped on the _scripts/gitHooks_ folder with the names descibes in the [Customizing Git - Git Hooks](https://git-scm.com/book/uz/v2/Customizing-Git-Git-Hooks) documentation will be installed with the [Install Command](#install)

you must follow this template to define a hook

``` javascript
#!/usr/bin/env node

const npmCommand = /^win/.test(process.platform) ? 'npm.cmd' : 'npm';
const {spawn} = require('child_process');

npm = spawn(npmCommand, ['run', 'lint']);
npm.stdout.on('data', (data) => {
    console.log(`${data}`);
});

npm.stderr.on('data', (data) => {
    console.log(`${data}`);
});

npm.on('close', (code) => {
    if (0 !== code) {
        console.log(`PreCommit Failed`);
        process.exitCode = 1;
    }
});
```

## Uninstall

This command delete the hooks from the _.git/hooks_ folder

``` shell
$ dogh uninstall
```

### Options

- Remove

    For removing the _scripts/gitHooks_ folder

    ``` shell
    $ dogh uninstall --remove
    ```

