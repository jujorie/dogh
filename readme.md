# DOGH - DevOps Git Hooks

Create and Manage Git Hooks using Node

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Documentation](#documentation)

## Installation

### Globally

To install the command line interface Globally

```shell
$ npm install -G dogh
```

after a successfuly install you can run the _dogh_ like

```shell
$ dogh
```

### Locally

To install the command line interface on your __node_modules__ folder

```shell
$ npm install -D dough
```

## Usage

```shell

Usage: dogh [options] [command]

DevOps Git hooks handler for projects

Options:
  -V, --version  output the version number
  -h, --help     output usage information

Commands:
  install        Install the git hooks on the project
  uninstall      Uninstall hooks

```

## Documentation

- [Cli Commands](docs/readme.md)