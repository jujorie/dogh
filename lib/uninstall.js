'use strict';

((module) => {
    const {unlinkSync, readdirSync, rmdirSync} = require('fs');
    const {processFile} = require('./utils/fileUtils');

    /**
     * Uninstall hocks
     * @param {{gitHooksPath:string,localHooksPath:string}} config
     *  - config values
     * @param {commander} commander - Commander object
     */
    function uninstall({
        gitHooksPath,
        localHooksPath,
    }, commander) {
        console.log('Uninstall');
        processFile(localHooksPath, gitHooksPath,
            (localHookFile) => {
                if (commander.remove) {
                    console.log('removing', localHookFile);
                    unlinkSync(localHookFile);
                }
            }
        );
        if (!readdirSync(localHooksPath).length) {
            rmdirSync(localHooksPath);
        }
    }

    module.exports = {
        description: 'Uninstall hooks',
        options: [
            {
                flags: '-r, --remove',
                description: 'remove the script folder',
            },
        ],
        action: uninstall,
    };
})(module);
