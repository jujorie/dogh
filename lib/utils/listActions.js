'use strict';
/* eslint max-len:0 */
((module) => {
    const {readdirSync} = require('fs');
    const {basename, extname, join} = require('path');
    const thisBasename = 'cli.js';

    /**
     * @typedef commandOption
     * @property {string} flags - Options flags
     * @property {string} description - Options descriptions
     */

    /**
     * Callack called when found an action
     * @callback listCommandsCallback
     * @param {string} actionName - Name of the action
     * @param {{description:string, action:function, options:Array<commandOption>}} action
     *  - Action to be added
     */

    /**
     * List actiond found on directory
     * @param {string} basePath - Base path
     * @param {listCommandsCallback} cb - callback
     *
     */
    function listActions(basePath, cb) {
        readdirSync(basePath)
            .filter((file) => file !== thisBasename
                && extname(file) === '.js')
            .forEach((file) => {
                const action = require(join(basePath, file));
                cb(basename(file, '.js'), action);
            });
    }

    module.exports = listActions;
})(module);
