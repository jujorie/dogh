'use strict';

((module) => {
    const {join} = require('path');
    const mkdirp = require('mkdirp');
    const {
        readdirSync,
        unlinkSync,
        existsSync,
    } = require('fs');

    /**
     * Callback for process files
     * @callback processFileCallback
     * @param {string} sourceFile - source file path
     * @param {string} destFile - destination file path
     */

    /**
     * Proccess files
     * @param {string} source - Source path
     * @param {string} dest - Destination path
     * @param {processFileCallback} cb - Callback
     */
    function processFile(source, dest, cb) {
        if (!existsSync(dest)) {
            mkdirp(dest);
        }
        readdirSync(source)
            .forEach((filename) => {
                const sourceFile = join(source, filename);
                const destFile = join(dest, filename);
                if (existsSync(destFile)) {
                    unlinkSync(destFile);
                }
                console.log(`* ${filename}`);
                cb(sourceFile, destFile);
            });
    }

    module.exports = {
        processFile,
    };
})(module);
