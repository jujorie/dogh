'use strict';

((module)=>{
    const {resolve} = require('path');

    /**
     * git hook path
     * @type {string}
     */
    const gitHooksPath = resolve('.git', 'hooks');

    /**
     * Local scripts path
     * @type {string}
     */
    const localHooksPath = resolve('scripts', 'githooks');

    /**
     * sample hooks path
     * @type {string}
     */
    const sampleHooksPath = resolve(__dirname, '../../hooks');

    module.exports = {
        gitHooksPath,
        localHooksPath,
        sampleHooksPath,
    };
})(module);
