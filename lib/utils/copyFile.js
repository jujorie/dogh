'use strict';
((module) => {
    const fs = require('fs');

    const SIZE = 65536;

    /**
     * copy file sync
     * @param {*} src - source file
     * @param {*} dest - destination file
     */
    function copyFile(src, dest) {
        let buffer;
        let i;
        let position = 0;
        const {
            size,
            mode,
        } = fs.statSync(src);

        const fdSrc = fs.openSync(src, 'r');
        const fdDest = fs.openSync(dest, 'w', mode);
        const length = size < SIZE ? size : SIZE;
        const peaceSize = size < SIZE ? 0 : size % SIZE;
        const offset = 0;

        buffer = Buffer.allocUnsafe(length);
        for (
            i = 0;
            length + position + peaceSize <= size;
            (i++), position = length * i
        ) {
            fs.readSync(fdSrc, buffer, offset, length, position);
            fs.writeSync(fdDest, buffer, offset, length, position);
        }

        if (peaceSize) {
            const length = peaceSize;
            buffer = Buffer.allocUnsafe(length);
            fs.readSync(fdSrc, buffer, offset, length, position);
            fs.writeSync(fdDest, buffer, offset, length, position);
        }

        fs.closeSync(fdSrc);
        fs.closeSync(fdDest);
    }


    module.exports = copyFile;
})(module);
