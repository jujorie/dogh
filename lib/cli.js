'use strict';

(() => {
    const commander = require('commander');
    const chalk = require('chalk');

    const pkg = require('../package');
    const listActions = require('./utils/listActions');
    const config = require('./utils/config');

    commander
        .name('dogh')
        .description(pkg.description)
        .version(pkg.version);

    listActions(__dirname, (actionName, action) => {
        const command = commander
            .command(actionName)
            .description(action.description || '')
            .action((cmd) => {
                action.action(config, cmd);
            });

        if (action.options && action.options.length) {
            action.options.forEach((option) => {
                command.option(option.flags, option.description);
            });
        };
    });

    try {
        commander.parse(process.argv);
        if (!commander.args.length) {
            throw new Error('No command specified, run dogh --help for help');
        }
    } catch (err) {
        console.error(chalk.red('** Error **'));
        console.error(err.message);
    }
    console.log('Done!');
})();
