'use strict';
/* eslint max-len:off */
((module) => {
    const mkdirp = require('mkdirp');
    const {existsSync, symlinkSync} = require('fs');

    const copyFile = require('./utils/copyFile');
    const {processFile} = require('./utils/fileUtils');

    /**
     * Install hooks hocks
     * @param {{gitHooksPath:string,localHooksPath:string,sampleHooksPath:string}}
     * config - config values
     */
    function installAction({
        gitHooksPath,
        localHooksPath,
        sampleHooksPath,
    }) {
        if (!existsSync(gitHooksPath)) {
            throw new Error(
                `Can't find git hooks path. ` +
                `Have you initialized the repo?`
            );
        };

        if (!existsSync(localHooksPath)) {
            console.log('No hooks folder found. creating');
            mkdirp.sync(localHooksPath);
            processFile(sampleHooksPath, localHooksPath, (source, dest) => {
                copyFile(source, dest);
            });
        }

        console.log('Install');
        processFile(localHooksPath, gitHooksPath,
            (scriptHookFile, gitHookFile) => {
                symlinkSync(scriptHookFile, gitHookFile);
            }
        );
    }

    module.exports = {
        description: 'Install the git hooks on the project',
        action: installAction,
    };
})(module);
